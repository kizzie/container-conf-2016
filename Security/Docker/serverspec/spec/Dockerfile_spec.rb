require "docker"
require "serverspec"

describe "Dockerfile" do
  before(:all) do
    @image = Docker::Image.build_from_dir('.')
    set :os, family: :debian
    set :backend, :docker
    set :docker_image, @image.id
  end

  it "installs Ubuntu 14.04" do
    expect(command("lsb_release -a").stdout).to include("Ubuntu 14.04")
  end

  describe package('apache2') do
    it { should be_installed }
  end

  describe service('apache2') do
    it {should be_running}
    it {should be_enabled}
  end

  describe package('git') do
    it { should_not be_installed }
  end

  describe 'Dockerfile#config' do
   it 'should expose port 80' do
     expect(@image.json['ContainerConfig']['ExposedPorts']).to include("80/tcp")
   end

   it 'should not expose random ports' do
     expect(@image.json['ContainerConfig']['ExposedPorts']).to contain_exactly(["80/tcp", {}])
   end
  end

end
